const adminBasePath = "/admin";
export const AdminDashboardPath = adminBasePath + "/dashboard";
export const AdminBuilderPath = adminBasePath + "/builder";
export const AdminMyPagePath = adminBasePath + "/my-page";
export const AdminGoogleMaterialPagePath = adminBasePath + "/google-material";
export const AdminReactBoostrapPath = adminBasePath + "/react-bootstrap";
export const AdminECommercePath = adminBasePath + "/e-commerce";
