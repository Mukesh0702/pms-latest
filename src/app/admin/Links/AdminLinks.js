import { RouteNest } from "app/routes/NestedRoutes";
import Buy from "app/users/Scenes/Buy";
import Home from "app/users/Scenes/Home";
import ErrorsPage from "app/modules/ErrorsExamples/ErrorsPage";
import React, { Suspense } from "react";
// import loadable from 'react-loadable';
// import Loader from '../Components/Common/Loader'
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import { LayoutSplashScreen, ContentRoute } from "_metronic/layout";
import { BuilderPage } from "app/pages/BuilderPage";
import { MyPage } from "app/pages/MyPage";
import { DashboardPage } from "app/pages/DashboardPage";
import {
  AdminDashboardPath,
  AdminBuilderPath,
  AdminMyPagePath,
} from "./AdminPaths";
//import { Logout, AuthPage } from "app/modules/Auth";
import { UserBuyPath, UserHomePath } from "app/users/Links/UserPaths";

function AdminLinks() {
  return (
    <>
      {/* <RouteNest title="" path="/auth/login" component={AuthPage} /> */}
      <RouteNest
        title=""
        isAdmin
        path={AdminDashboardPath}
        component={DashboardPage}
      />
      <RouteNest
        title=""
        isAdmin
        path={AdminBuilderPath}
        component={BuilderPage}
      />
      <RouteNest title="" isAdmin path={AdminMyPagePath} component={MyPage} />
      {/* <Redirect from="/auth" to="/auth/login" /> */}
    </>
  );
}

export default AdminLinks;
