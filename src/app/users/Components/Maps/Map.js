import React, { useState, useEffect } from "react";
import {
  withGoogleMap,
  withScriptjs,
  GoogleMap,
  Marker,
  InfoWindow,
} from "react-google-maps";
import * as dummyMapData from "app/data/dummyMapData.json";
import mapStyles from "app/helpers/mapStyles";

function Map() {
  const [selectedMapData, setSelectedMapData] = useState(null);

  useEffect(() => {
    const listener = (e) => {
      if (e.key === "Escape") {
        setSelectedMapData(null);
      }
    };
    window.addEventListener("keydown", listener);

    return () => {
      window.removeEventListener("keydown", listener);
    };
  }, []);

  return (
    <GoogleMap
      defaultZoom={10}
      defaultCenter={{ lat: 27.708796, lng: 85.320244 }}
      defaultOptions={{ styles: mapStyles }}
    >
      {dummyMapData.features.map((park) => (
        <Marker
          key={park.properties.PARK_ID}
          position={{
            lat: park.geometry.coordinates[0],
            lng: park.geometry.coordinates[1],
          }}
          onClick={() => {
            setSelectedMapData(park);
          }}
          icon={{
            url: `/assets/icons/marker.svg`,
            scaledSize: new window.google.maps.Size(25, 25),
          }}
        />
      ))}

      {selectedMapData && (
        <InfoWindow
          onCloseClick={() => {
            setSelectedMapData(null);
          }}
          position={{
            lat: selectedMapData.geometry.coordinates[1],
            lng: selectedMapData.geometry.coordinates[0],
          }}
        >
          <div>
            <h2>{selectedMapData.properties.NAME}</h2>
            <p>{selectedMapData.properties.DESCRIPTIO}</p>
          </div>
        </InfoWindow>
      )}
    </GoogleMap>
  );
}

const MapWrapped = withScriptjs(withGoogleMap(Map));

export default function App() {
  console.log(process.env.REACT_APP_GOOGLE_KEY);
  return (
    <div style={{ width: "580px", height: "500px" }}>
      <MapWrapped
        googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${process.env.REACT_APP_GOOGLE_KEY}`}
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `100%` }} />}
        mapElement={<div style={{ height: `100%` }} />}
      />
    </div>
  );
}
