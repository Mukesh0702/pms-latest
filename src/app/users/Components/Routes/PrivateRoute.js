import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Redirect, Route } from "react-router-dom";
import isEmpty from "app/helpers/isEmpty";

const PrivateRoute = ({
  component: Component,
  children,
  title,
  auth,
  render,
  isNotPrivate,
  isAdmin,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        // return auth.isAuthenticated === true || isNotPrivate (
        //     <Component {...props} ref={ref=>!isEmpty(rest.childRef) ? rest.childRef.current = ref:null} children={children} />
        // ): (
        //     <Redirect to={{pathname:"/login", state: { auth: false }}} />
        //     // window.open(getBaseAxios()),<Redirect to={{pathname:"/login", state: { auth: false }}} />
        // )
        return (
          <ShowRoutes
            {...props}
            auth={auth}
            isNotPrivate={isNotPrivate}
            children={children}
            Component={Component}
            isAdmin={isAdmin}
            title={title}
            ref={(ref) =>
              !isEmpty(rest.childRef) ? (rest.childRef.current = ref) : null
            }
          />
        );
      }}
    />
  );
};

export const ShowRoutes = ({
  Component,
  children,
  title,
  auth,
  ref,
  isNotPrivate,
  isAdmin,
  ...rest
}) => {
  useEffect(() => {
    const {
      location: { pathname },
    } = rest;
    if (!isEmpty(title)) {
      document.title = title;
    } else {
      let title = pathname.split("/");
      if (!isEmpty(title) && title.length > 0 && Array.isArray(title)) {
        title = title[title.length - 1].toUpperCase();
      } else {
        title = "PMS";
      }
      document.title = title;
    }
  }, []);
  return <Component {...rest} ref={(ref) => ref} children={children} />;
};

PrivateRoute.propTypes = {
  //auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  // auth: state.auth,
});
export default connect(mapStateToProps)(PrivateRoute);
