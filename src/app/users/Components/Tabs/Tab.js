import React, { Component } from "react";
import PropTypes from "prop-types";

class Tab extends Component {
  static propTypes = {
    activeTab: PropTypes.string.isRequired,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
    onClick: PropTypes.func.isRequired,
  };

  onClick = () => {
    const { name, onClick, id, onOutsideClick } = this.props;
    onClick(name, id);
    onOutsideClick && onOutsideClick();
  };

  render() {
    const {
      onClick,
      props: { activeTab, label, name, isBlock },
    } = this;
    let className = isBlock ? "tabs-block__item" : "tabs-list__item";

    if (activeTab === name) {
      className += " tab-active";
    }

    return (
      <li name={name} className={className} onClick={() => onClick()}>
        {label}
      </li>
    );
  }
}

export default Tab;
