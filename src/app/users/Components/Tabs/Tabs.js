import React, { Component } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Tab from "./Tab";
import isEmpty from "app/helpers/isEmpty";
import { MdKeyboardArrowDown } from "react-icons/md";

class Tabs extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Array).isRequired,
    type: PropTypes.string,
    color: PropTypes.string,
    noBackground: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    this.state = {
      // activeTab: this.props.children[0].props.label,
      expand: false,
      activeTab: "",
    };
    this.onClickTabItem = this.onClickTabItem.bind(this);
  }

  componentDidUpdate() {
    //debugger;
    if (
      !isEmpty(this.props.activeTab) &&
      this.state.activeTab !== this.props.activeTab
    ) {
      this.setState({ activeTab: this.props.activeTab });
    }
  }
  onClickTabItem = (tab, id) => {
    //debugger;
    //if (event.target.id !== this.props.closeIconId) {
    this.setState({ activeTab: tab });
    if (!isEmpty(this.props.onClickTabItem)) this.props.onClickTabItem(id);
    //}
  };

  render() {
    //debugger;
    const {
      onClickTabItem,
      props: { children, type, isCollapse, color, noBackground },
      state: { activeTab },
    } = this;

    var isActive = false;
    var actTab = "";
    //debugger;
    let isSingle = !Array.isArray(children) && typeof children === "object";
    if (isSingle) {
      this.setState({ activeTab: this.props.children.props.name });
    } else if (
      !isEmpty(activeTab) &&
      children.length === 1 &&
      activeTab !== this.props.children[0].props.name
    ) {
      this.setState({ activeTab: this.props.children[0].props.name });
    }
    // else if (
    //   !isEmpty(this.props.activeTab) &&
    //   activeTab !== this.props.children[0].props.name
    // ) {
    //   this.setState({ activeTab: this.props.activeTab });
    // }
    else {
      if (isEmpty(this.state.activeTab)) {
        children.map((child) => {
          if (!isEmpty(child)) {
            if (!isActive) {
              if (child.props.active) {
                isActive = true;
                actTab = child.props.name;
              } else {
                isActive = false;
                actTab = "";
              }
            }
          }
        });
        if (isActive) {
          this.setState({ activeTab: actTab });
        } else {
          this.setState({
            activeTab: this.props.children[0].props.name,
          });
        }
      }
    }

    const collapseIconClass = classnames({
      "tabs-block__item": true,
      "tabs-collapse-icon": isCollapse && this.state.expand === true,
      "tabs-expand-icon": isCollapse && this.state.expand === false,
    });
    const collapseClass = classnames({
      "tabs-block": true,
      "tabs-collapse": isCollapse && this.state.expand === true,
      "tabs-expand": isCollapse && this.state.expand === false,
    });
    const tabsClass = classnames({
      tabs: true,
      "tabs-primary": color === "primary",
      "tabs-danger": color === "danger",
      "tabs-success": color === "success",
      "tabs-warning": color === "warning",
      "tabs-no-bg": noBackground,
    });
    const tabType =
      type === "tabs-block" ? (
        <div className="tabs-card">
          <div className={collapseClass}>
            <ol className="tabs-block__list">
              {children.map((child) => {
                if (!isEmpty(child)) {
                  const { label, name, id, onClick } = child.props;
                  return (
                    <Tab
                      activeTab={activeTab}
                      key={id}
                      id={id}
                      name={name}
                      label={label}
                      onOutsideClick={!isEmpty(onClick) ? onClick : null}
                      onClick={onClickTabItem}
                      isBlock
                    />
                  );
                }
              })}
              {isCollapse ? (
                <li
                  className={collapseIconClass}
                  onClick={() =>
                    this.setState({
                      expand: !this.state.expand,
                    })
                  }
                >
                  <MdKeyboardArrowDown />
                </li>
              ) : null}
            </ol>
            <div className="tabs-content">
              {children.map((child) => {
                if (child.props.name !== activeTab) return undefined;
                return child.props.children;
              })}
            </div>
          </div>
        </div>
      ) : (
        <div className={tabsClass}>
          <ul className="tabs-list">
            {children.map((child) => {
              if (!isEmpty(child) && !child.props.isFooter) {
                const { label, name, id, onClick } = child.props;

                return (
                  <Tab
                    activeTab={activeTab}
                    key={id}
                    id={id}
                    name={name}
                    label={label}
                    onOutsideClick={!isEmpty(onClick) ? onClick : null}
                    onClick={onClickTabItem}
                  />
                );
              }
            })}
          </ul>
          <div className="tabs-content">
            {children.map((child) => {
              if (!isEmpty(child)) {
                const childElement = React.Children.only(child.props.children);
                if (child.props.name !== activeTab) return undefined;
                return React.cloneElement(childElement);
              }
            })}
          </div>
          {children.map((child, i) => {
            if (!isEmpty(child) && child.props.isFooter) {
              return (
                <div className="tabs-footer" key={i}>
                  {child.props.children}
                </div>
              );
            }
          })}
        </div>
      );
    return tabType;
  }
}

export default Tabs;
