import React from "react";

function RecentSearch(props) {
  const recentSearch = [
    {
      Id: 1,
      Name: "Dhapasi Height",
      Type: "All listing",
      Count: 6,
    },
    {
      Id: 2,
      Name: "Balkhu ",
      Type: "House",
      Count: 10,
    },
  ];
  return (
    <div className="recent-search">
      <div className="recent-search__header">
        <svg viewBox="0 0 24 24" className="domain-icon" aria-hidden="true">
          <path
            fill="none"
            stroke="currentColor"
            stroke-width="2"
            d="M17 4v4m4-4h-4m.5.9A9 9 0 1 1 12 3l1.8.2"
          ></path>
          <path
            fill="none"
            stroke="currentColor"
            d="M3.5 12.5h2m13 0h2m-9 7.5v-1.5m0-13V3m0 5.5V12m2 1.5l-2-1.5"
          ></path>
        </svg>
        <span className="header-text">RECENT SEARCHES</span>
      </div>
      <div className="recent-search__blocks">
        {recentSearch.map((x) => {
          return (
            <div className="recent-search-block">
              <span className="name">{x.Name}</span>
              <span className="type">{x.Type}</span>
              <span className="count">{x.Count}</span>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default RecentSearch;
