import Slider from "@material-ui/core/Slider";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import React from "react";

const useStyles = makeStyles((theme) => ({
  root: {
    width: 300 + theme.spacing(3) * 2,
  },
  margin: {
    height: theme.spacing(5),
  },
}));

function valuetext(value) {
  return `${value}`;
}

const PrettoSlider = withStyles({
  root: {
    color: "#dc5a5f",
    height: 18,
    //marginTop: 48,
  },
  thumb: {
    height: 38,
    width: 38,
    backgroundColor: "currentColor",
    border: "2px solid currentColor",
    marginTop: -8,
    marginLeft: -12,
    "&:focus, &:hover, &$active": {
      boxShadow: "inherit",
    },
  },
  active: {},
  valueLabel: {
    left: "calc(-50% + 16px)",
  },

  track: {
    height: 25,
    borderRadius: 4,
    //marginRight:30
   // backgroundColor: "#26b01940",
    backgroundColor: "#f5797e",
  },
  rail: {
    height: 25,
    borderRadius: 4,
    //marginLeft:30,
    backgroundColor: "#ffcc0040",
  },
  mark: {
    backgroundColor: "#bfbfbf",
    height: 24,
    width: 1,
    marginTop: 0,
    display: "none",
  },
  markActive: {
    opacity: 1,
    backgroundColor: "currentColor",
    color: "currentColor",
  },
  markLabel: {
    marginTop: -10,
    color: "#000",
    fontWeight: "bold",
    //margin:"0 15px"
  },
  markLabelActive: {
    color: "currentColor",
  },
})(Slider);

function ThumbComponent(props) {
  return (
    <span {...props}>
      <span style={{ color: "#fff", fontWeight: "bold" }}>
        {props["aria-valuenow"]}
      </span>
    </span>
  );
}

export default function CustomizedSlider(props) {
  const {
    step,
    marks,
    min,
    max,
    defaultValue,
    valueLabelDisplay,
    name,
    onChange,
    value,
  } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <PrettoSlider
        name={name}
        value={value}
        defaultValue={defaultValue}
        getAriaValueText={valuetext}
        aria-labelledby=""
        step={step}
        min={min}
        max={max}
        marks={marks}
        valueLabelDisplay={valueLabelDisplay}
        valueLabelFormat={"212"}
        ThumbComponent={ThumbComponent}
        onChange={(event, value) => {
          onChange(name, value);
        }}
      />
    </div>
  );
}
