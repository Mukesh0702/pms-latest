import React from "react";
import { IoIosArrowBack } from "react-icons/io";
import classNames from "classnames";

function PrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      //   className={className}
      //   style={{ ...style, display: "block", background: "red" }}
      className={classNames("arrow", " arrow-left", className)}
      onClick={onClick}
    >
      <IoIosArrowBack className="arrow-icon" />
    </div>
  );
}

export default PrevArrow;
