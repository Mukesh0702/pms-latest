import React from "react";
import { IoIosArrowForward } from "react-icons/io";
import classNames from "classnames";

function NextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      //   className={className}
      //   style={{ ...style, display: "block", background: "red" }}
      className={classNames("arrow", " arrow-right", className)}
      onClick={onClick}
    >
      <IoIosArrowForward className="arrow-icon" />
    </div>
  );
}

export default NextArrow;
