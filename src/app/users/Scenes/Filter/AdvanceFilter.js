import React, { Component } from "react";
import SearchTabs from "app/users/Scenes/Home/SearchTabs";
import SearchFilter from "app/users/Components/Common/SearchFilter";
import "./advanceFilter.scss";
import AdvanceFilterForm from "./AdvanceFilterForm";
import { GrClose } from "react-icons/gr";
import SearchButton from "app/users/Components/Common/SearchButton";

class AdvanceFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabs: [
        {
          id: 1,
          label: "Buy",
          component: (
            <>
              <SearchFilter />
            </>
          ),
        },
        {
          id: 2,
          label: "Rent",
          component: (
            <>
              <SearchFilter />
            </>
          ),
        },
        {
          id: 3,
          label: "New Homes",
          component: (
            <>
              <SearchFilter />
            </>
          ),
        },
        {
          id: 4,
          label: "Sold",
          component: (
            <>
              <SearchFilter />
            </>
          ),
        },
        {
          id: 5,
          label: "Rural",
          component: (
            <>
              <SearchFilter />
            </>
          ),
        },
      ],
      showFooterButton: true,
    };
  }
  componentDidMount() {
    let el = {};
    el.target = document.getElementById("advanceFilter");
    console.log(el.target.scrollHeight, window.innerHeight);
    if (el.target.scrollHeight > window.innerHeight + 100)
      this.handleScroll(el);
  }
  handleScroll = (e) => {
    let headerEl = document.getElementById("advanceFilterHeader");
    const isScrolledMorethanScreen =
      e.target.scrollTop >= headerEl.clientHeight - 100;

    this.setState({ showFooterButton: isScrolledMorethanScreen });
  };
  render() {
    const { tabs, showFooterButton } = this.state;
    return (
      <div
        className="advance-filter"
        onScroll={this.handleScroll}
        id="advanceFilter"
      >
        <div className="advance-filter__header" id="advanceFilterHeader">
          <div className="header-close">
            <GrClose
              onClick={() => this.props.handleAdvanceFilterShow(false)}
              className="close-icon"
            />
          </div>
          <SearchTabs tabs={tabs} classes={["advance-filter__search-tabs"]} />
        </div>
        <div className="advance-filter__body">
          <AdvanceFilterForm />
        </div>
        {showFooterButton && (
          <div className="advance-filter__footer">
            <SearchButton />
          </div>
        )}
      </div>
    );
  }
}

export default AdvanceFilter;
