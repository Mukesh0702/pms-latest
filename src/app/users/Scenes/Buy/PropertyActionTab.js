import classNames from "classnames";
import React, { useState } from "react";
import { IoIosMap, IoIosListBox, IoIosCalendar } from "react-icons/io";

function PropertyActionTab(props) {
  const tabs = [
    { Id: 1, Name: "List View", Icon: IoIosListBox },
    { Id: 2, Name: "Map View", Icon: IoIosMap },
    { Id: 3, Name: "Inspection/Auction", Icon: IoIosCalendar },
  ];
  let { selectedTabId, handleSetSelectedTabId, classes } = props;
  return (
    <div className={classNames("property-tab", classes)}>
      {tabs.map((x) => {
        return (
          <div
            className={classNames({
              "property-tab__list": true,
              flex: true,
              active: selectedTabId === x.Id,
            })}
            onClick={() => handleSetSelectedTabId(x.Id)}
          >
            <x.Icon />
            <span className="tab-item-text">{x.Name}</span>
          </div>
        );
      })}
    </div>
  );
}

export default PropertyActionTab;
