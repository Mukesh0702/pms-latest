import React, { Component } from "react";
import { IoIosArrowForward } from "react-icons/io";
import PropertyActionTab from "./PropertyActionTab";
import PropertyBlock from "./PropertyBlock";
import "./buy.scss";
import Maps from "app/users/Components/Maps/Map";
import FilterBar from "./FilterBar";

class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTabId: 1,
    };
  }
  handleSetSelectedTabId = (id) => {
    this.setState({ selectedTabId: id });
  };
  render() {
    const { selectedTabId } = this.state;
    const tabs = [
      {
        id: 1,
        label: "Details",
        component: <></>,
      },
      {
        id: 2,
        label: "Map",
        component: <></>,
      },
      {
        id: 3,
        label: "dsdd",
        component: <></>,
      },
    ];
    return (
      <div className="buy">
        <FilterBar />
        <div className="pathname flex">
          <span className="root" onClick={()=>this.props.history.push('/home')}>Home</span>
          <IoIosArrowForward />
          <span>Buy</span>
        </div>
        <div className="buy-content">
          <div className="buy-content__left">
            <div className="property-details">
              <h1 data-testid="summary" class="property-summary">
                <strong>35 Properties</strong> for sale
              </h1>
            </div>

            <PropertyActionTab
              tabs={tabs}
              handleSetSelectedTabId={this.handleSetSelectedTabId}
              selectedTabId={selectedTabId}
            />
            <PropertyBlock />
          </div>
          <div className="buy-content__right">
            {selectedTabId === 2 && <Maps />}
          </div>
        </div>
      </div>
    );
  }
}

export default index;
