import React, { Component } from "react";
import { IoIosArrowDown } from "react-icons/io";
import { RiSearch2Line } from "react-icons/ri";
import classNames from "classnames";
class FilterBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeFilter: 2,
      filterItems: [
        { Id: 1, Name: "Filters" },
        { Id: 2, Name: "Buy" },
        { Id: 3, Name: "Price" },
        { Id: 4, Name: "Beds" },
        { Id: 5, Name: "Property types" },
      ],
    };
  }
  handleFilterItemClick = (id) => {
    this.setState({ activeFilter: id });
  };
  render() {
    const { activeFilter, filterItems } = this.state;
    return (
      <div className="filter-bar">
        <div className="search-bar">
          <div className="search-box">
            <input
              type="text"
              placeholder="Try a location or school you want to live by "
            />
            <div className="search-button  flex">
              <RiSearch2Line />
            </div>
            {/* <button
                type="button"
                className="search-button button with-icon button-primary"
                onClick={() => this.props.history.push("/buy")}
              >
                <RiSearch2Line />
                <span>Search</span>
              </button> */}
          </div>
          <div className="filter-bar__items">
            <div className="filter-inner-bar">
              {filterItems.map((x) => {
                return x.Id === 1 ? (
                  <button
                    type="button"
                    className={classNames({
                      "filter-items": true,
                      active: activeFilter === x.Id,
                    })}
                    onClick={() => this.handleFilterItemClick(x.Id)}
                  >
                    <span>
                      <svg
                        viewBox="0 0 24 24"
                        aria-hidden="true"
                        class="domain-icon css-jeyium"
                      >
                        <path
                          fill="none"
                          stroke="currentColor"
                          stroke-width="2"
                          d="M3 19h2m6 0h10"
                        ></path>
                        <circle
                          cx="8"
                          cy="19"
                          r="2.5"
                          fill="none"
                          stroke="currentColor"
                        ></circle>
                        <path
                          fill="none"
                          stroke="currentColor"
                          stroke-width="2"
                          d="M3 5h2m6 0h10"
                        ></path>
                        <circle
                          cx="8"
                          cy="5"
                          r="2.5"
                          fill="none"
                          stroke="currentColor"
                        ></circle>
                        <path
                          fill="none"
                          stroke="currentColor"
                          stroke-width="2"
                          d="M21 12h-2m-6 0H3"
                        ></path>
                        <circle
                          cx="16"
                          cy="12"
                          r="2.5"
                          fill="none"
                          stroke="currentColor"
                        ></circle>
                      </svg>
                    </span>
                    <span className="pl-xsm">{x.Name}</span>
                  </button>
                ) : (
                  <button
                    type="button"
                    className={classNames({
                      "filter-items": true,
                      active: activeFilter === x.Id,
                    })}
                    onClick={() => this.handleFilterItemClick(x.Id)}
                  >
                    <span>{x.Name}</span>
                    <span className="ml-xsm">
                      <IoIosArrowDown className="drop-icon" />
                    </span>
                  </button>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FilterBar;
