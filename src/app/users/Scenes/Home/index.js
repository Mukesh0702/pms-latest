import BuyBlock from "app/users/Components/Common/BuyBlock";
import HomeContent from "app/users/Layouts/HomeContent";
import React from "react";
import NewDevelopment from "./NewDevelopment";

function Home(props) {
  return (
    <div className="home">
      <div className="top-section">
        <div className="nav-bar-bag">
          <div className="nav-bar-bag__item"></div>
        </div>
        <HomeContent />
        <NewDevelopment />
        <BuyBlock />
      </div>
      {/* <MapWithAnOverlayView
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbz7EJJz9871a3S7A1GontzrEHmH8U_1w&v=3.exp&libraries=geometry,drawing,places"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `400px` }} />}
        mapElement={<div style={{ height: `100%` }} />}
      /> */}
    </div>
  );
}

export default Home;
