import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default class NewDevelopment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newDevelopment: [
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
        {
          Id: 1,
          PropertyCount: 8,
          Name: "Shangrilla Housing",
          Address: "Sano thimi, Kathmandu Nepal",
        },
      ],
    };
  }

  render() {
    var settings = {
      dots: true,
      infinite: false,
      speed: 1000,
      slidesToShow: 3,
      slidesToScroll: 3,
      initialSlide: 0,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };
    return (
      <div className="new-development">
        <h2 className="new-development__header"> New Developments </h2>
        <Slider {...settings}>
          {this.state.newDevelopment.map((x) => {
            return (
              <div className="new-development__block">
                <div className="inner-block">
                  <img
                    src="https://rimh2.domainstatic.com.au/syTDv8kTMGnKuKgimvPqcjom7Tk=/648x452/filters:format(jpeg):quality(80):no_upscale()/a59a357c-82dd-4fbc-9362-3747fdbf56ed-w1600-h1200"
                    alt=""
                    className="inner-block__image"
                  />
                  <span className="inner-block__prop-count">
                    {x.PropertyCount} Properties
                  </span>
                  <div className="inner-block__name-add">
                    <span className="name">{x.Name}</span>
                    <span className="address">{x.Address}</span>
                  </div>
                </div>
              </div>
            );
          })}
        </Slider>
      </div>
    );
  }
}
