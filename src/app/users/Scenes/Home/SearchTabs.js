import React from "react";
import Tabs from "app/users/Components/Tabs/Tabs";
import classNames from "classnames";

function searchTabs({ tabs, classes }) {
  return (
    <div className={classNames("search-tab", classes)}>
      <Tabs
        color="primary"
        isCollapse={false}
        onClickTabItem={(id) => console.log(id)}
        //activeTab={this.state.activeTabLabel}
        closeIconId="closeIcon"
      >
        {tabs &&
          tabs.map((ct, i) => {
            return (
              <div
                key={ct.id}
                name={ct.label}
                label={ct.label}
                //active={ct.id === selectedCreateTabId}
              >
                {ct.component}
              </div>
            );
          })}
      </Tabs>
    </div>
  );
}

export default searchTabs;
