import React from "react";
import SearchTabs from "app/users/Scenes/Home/SearchTabs";
import SearchFilter from "app/users/Components/Common/SearchFilter";
import RecentSearch from "app/users/Components/Common/RecentSearch";

function HomeContent(props) {
  const tabs = [
    {
      id: 1,
      label: "Buy",
      component: (
        <>
          <SearchFilter />
          <RecentSearch />
        </>
      ),
    },
    {
      id: 2,
      label: "Rent",
      component: (
        <>
          <SearchFilter />
          <RecentSearch />
        </>
      ),
    },
    {
      id: 3,
      label: "New Homes",
      component: (
        <>
          <SearchFilter />
          <RecentSearch />
        </>
      ),
    },
    {
      id: 4,
      label: "Sold",
      component: (
        <>
          <SearchFilter />
          <RecentSearch />
        </>
      ),
    },
    {
      id: 5,
      label: "Rural",
      component: (
        <>
          <SearchFilter />
          <RecentSearch />
        </>
      ),
    },
  ];
  return (
    <div role="main" id="home-content" className="home-content">
      <header className="home-content__header">
        <h1 data-testid="headline" className="">
          Search Nepal's home of property
        </h1>
      </header>
      <div className="home-content__body">
        <SearchTabs tabs={tabs} />
      </div>
    </div>
  );
}

export default HomeContent;
