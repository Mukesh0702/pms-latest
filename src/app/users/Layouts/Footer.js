import React from "react";
import { GrInstagram, GrFacebook } from "react-icons/gr";
import { FiPhoneCall } from "react-icons/fi";
import { MdLocationOn, MdEmail } from "react-icons/md";
import { FaTwitter } from "react-icons/fa";

function Footer(props) {
  return (
    <div className="footer">
      <div className="about-us">
        <div className="about-us__header footer-header">
          <span>About Us</span>
        </div>
        <div className="about-us__body footer-body">
          <p>
            Sheetal Property is committed to delivering a high level of
            expertise, customer service, and attention to detail to the
            marketing and sales of luxury real estate, and rental properties.
          </p>
        </div>
      </div>
      <div className="contact-us">
        <div className="contact-us__header footer-header">
          <span>Contact Us</span>
        </div>
        <div className="contact-us__body footer-body">
          <div className="footer-body__row">
            <MdLocationOn className="text-bg" />
            <span className="pl-sm">Baluwater, Kathmandu</span>
          </div>
          <div className="footer-body__row">
            <FiPhoneCall className="text-bg" />
            <span className="pl-sm">Call us FREE +977 980100434</span>
          </div>
          <div className="footer-body__row">
            <MdEmail className="text-bg" />
            <span className="pl-sm"> info@sheetalproperty.com</span>
          </div>
        </div>
      </div>
      <div className="our-service">
        <div className="our-us__header footer-header">
          <span>Our Service</span>
        </div>
        <div className="our-us__body footer-body">
          <div className="footer-body__row">
            <span> Buy Properties</span>
          </div>
          <div className="footer-body__row">
            <span>Sell Properties</span>
          </div>
          <div className="footer-body__row">
            <span>Rent Properties</span>
          </div>
        </div>
      </div>
      <div className="social-platform">
        <div className="social-platform__header footer-header">
          <span>Social Platform</span>
        </div>
        <div className="social-platform__body footer-body">
          <div className="footer-body__row">
            <GrFacebook className="text-bg" />
            <span className="pl-sm">Facebook</span>
          </div>
          <div className="footer-body__row">
            <GrInstagram className="text-bg" />
            <span className="pl-sm">Instagram</span>
          </div>
          <div className="footer-body__row">
            <FaTwitter className="text-bg" />
            <span className="pl-sm">Twitter</span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
