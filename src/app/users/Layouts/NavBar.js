import React, { useEffect, useRef } from "react";
import classNames from "classnames";
import { UserHomePath } from "../Links/UserPaths";
import { useHistory } from "react-router-dom";
import { FaRegUserCircle } from "react-icons/fa";
import { MdKeyboardArrowDown } from "react-icons/md";
import { shallowEqual, useSelector } from "react-redux";
import isEmpty from "app/helpers/isEmpty";
import Avatar from "react-avatar";

function NavBar(props) {
  const { isUserAuthorized, user } = useSelector(
    ({ auth }) => ({
      isUserAuthorized: auth.user != null && auth.user.roles.includes(2),
      user: auth.user,
    }),
    shallowEqual
  );
  const onLiClick = (event) => {
    let target = event.target.closest(".list-item");
    let nextSibling = target.nextSibling;
    if (!nextSibling.classList.contains("active")) {
      removeActiveNavBar();
      nextSibling.classList.toggle("active");
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
  }, []);

  const handleClickOutside = (e) => {
    let target = e.target.closest(".list-item");
    if (target) return;
    else {
      var isRightMB;
      e = e || window.event;

      if ("which" in e)
        // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
        isRightMB = e.which == 3;
      else if ("button" in e)
        // IE, Opera
        isRightMB = e.button == 2;

      if (!isRightMB) removeActiveNavBar();
    }
  };

  const removeActiveNavBar = () => {
    var elems = document.querySelectorAll(".header-menu-desktop-dropdown");

    [].forEach.call(elems, function(el) {
      el.classList.remove("active");
    });
  };

  const isHomePage = props.location.pathname.split("/").includes("home");
  const history = useHistory();
  const logoutClick = () => {
    history.push("/logout");
  };
  const userImage = (
    <Avatar
      alt={user.fullname}
      name={user.fullname}
      src={user.pic}
      className="emp-img"
      round
      size={30}
    />
  );
  return (
    <header
      className={classNames({
        "nav-bar": true,
        "white-bar": isHomePage,
        "black-bar": !isHomePage,
      })}
    >
      <div className="nav-bar__toolbar">
        <button
          data-testid="mobile-nav__main-toggle"
          type="button"
          className="mobile-nav__main-toggle"
        >
          <span className="">Navigation</span>
          <span className=""></span>
        </button>
        <a href={UserHomePath} className="company-name">
          <span>Sheetal Property</span>
        </a>
        <button
          type="button"
          data-testid="mobile-nav__secondary-toggle"
          className="mobile-nav__secondary-toggle"
        >
          <div>
            <div>
              <svg viewBox="0 0 24 24" aria-hidden="true">
                <path
                  fill="currentColor"
                  d="M2.593 24v-2.3c0-6.093 6.426-6.093 7.615-7.684l.14-.728c-1.675-.846-2.854-2.885-2.854-5.267C7.494 4.88 9.54 3 12.064 3c2.52 0 4.567 1.877 4.567 5.02 0 2.362-1.15 4.39-2.806 5.245l.16.828c1.307 1.52 7.423 1.615 7.423 7.607V24H2.593z"
                ></path>
              </svg>
            </div>
          </div>
          <span>Account</span>
        </button>
        <nav className="navigation">
          {!isUserAuthorized ? (
            <div className="login">
              <button
                type="button"
                className="btn-login"
                onClick={() => history.push("/auth/login")}
              >
                Login
              </button>
            </div>
          ) : (
            <div className="logged-in">
              <div className="user-image flex list-item cursor-pointer" onClick={onLiClick}>
                {/* <FaRegUserCircle /> */}
                {userImage}
                <svg
                  viewBox="0 0 18 18"
                  aria-hidden="true"
                  className="domain-icon"
                >
                  <path
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    d="M13 7l-4 4-4-4"
                  ></path>
                </svg>
              </div>
              <div className="header-menu-desktop-dropdown">
                <div className="user-header">
                  <div className="flex">
                    <div class="user-header__icon">
                      {userImage}
                      {/* <div class="inner-icon">
                          <svg
                            viewBox="0 0 24 24"
                            aria-hidden="true"
                            class="domain-icon"
                          >
                            <path
                              fill="currentColor"
                              d="M2.593 24v-2.3c0-6.093 6.426-6.093 7.615-7.684l.14-.728c-1.675-.846-2.854-2.885-2.854-5.267C7.494 4.88 9.54 3 12.064 3c2.52 0 4.567 1.877 4.567 5.02 0 2.362-1.15 4.39-2.806 5.245l.16.828c1.307 1.52 7.423 1.615 7.423 7.607V24H2.593z"
                            ></path>
                          </svg>
                        </div> */}
                    </div>

                    <span class="user-header__email">{user.email}</span>
                  </div>
                </div>
                <ul className="user-menu-ul">
                  <li className="user-menu-ul__item">
                    <a href="https://www.domain.com.au/" target="_self">
                      <svg
                        viewBox="0 0 24 24"
                        class="domain-icon header-member__dropdown-item-link-icon common-svg "
                        aria-hidden="true"
                      >
                        <path
                          d="M11.4 17.2l-5.6 3.7a.5.5 0 0 1-.7-.7l2.8-5.5a1 1 0 0 0-.3-1.2L3.2 10a.5.5 0 0 1 .3-1h5.1a1 1 0 0 0 .9-.6l1.9-5a.6.6 0 0 1 1.1 0l1.9 5a1 1 0 0 0 .9.6h5.1a.5.5 0 0 1 .3 1l-4.4 3.5a1 1 0 0 0-.3 1.2l2.8 5.5a.5.5 0 0 1-.8.7l-5.6-3.7a1 1 0 0 0-1 0z"
                          fill="inherit"
                          stroke="currentColor"
                          stroke-miterlimit="10"
                          stroke-width="2"
                        ></path>
                      </svg>
                      <span>Shortlist</span>
                    </a>
                  </li>
                  <li className="user-menu-ul__item">
                    <a href="#" target="_self">
                      <svg
                        viewBox="0 0 24 24"
                        class="domain-icon header-member__dropdown-item-link-icon common-svg "
                        aria-hidden="true"
                      >
                        <circle
                          cx="10.5"
                          cy="10.5"
                          r="7.5"
                          fill="none"
                          stroke="currentColor"
                          stroke-width="2"
                        ></circle>
                        <path
                          fill="none"
                          stroke="currentColor"
                          stroke-width="2"
                          d="M16 16l5 5"
                        ></path>
                        <path
                          d="M14.3 8.5h-2.9l-.7-1.8a.3.3 0 0 0-.5 0l-.6 1.8H6.7a.2.2 0 0 0-.1.4l2.2 1.6-1 2.6a.3.3 0 0 0 .4.3l2.2-1.6 2.2 1.6a.3.3 0 0 0 .4-.3l-1-2.6 2.2-1.6a.2.2 0 0 0 .1-.4z"
                          fill="none"
                          stroke="currentColor"
                        ></path>
                      </svg>
                      <span>Saved Search</span>
                    </a>
                  </li>
                  <li
                    className="user-menu-ul__item list-item"
                    onClick={logoutClick}
                  >
                    <a
                      //href=""
                      target="_self"
                    >
                      <svg
                        viewBox="0 0 24 24"
                        class="domain-icon header-member__dropdown-item-link-icon common-svg "
                        aria-hidden="true"
                      >
                        <path
                          d="M13 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h8"
                          fill="none"
                          stroke="currentColor"
                          stroke-width="2"
                        ></path>
                        <path
                          d="M15.5 14.5V17a.5.5 0 0 0 .9.4l5-5a.5.5 0 0 0 0-.7l-5-5a.5.5 0 0 0-.9.4v2.4H8a.5.5 0 0 0-.5.5v4a.5.5 0 0 0 .5.5h7.5"
                          fill="none"
                          stroke="currentColor"
                        ></path>
                      </svg>
                      <span>Logout</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          )}
          <ul className="navigation__ul">
            <li className="navigation__li" onClick={onLiClick}>
              <a className="list-item">
                Find a Property
                <svg
                  viewBox="0 0 18 18"
                  aria-hidden="true"
                  className="domain-icon"
                >
                  <path
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    d="M13 7l-4 4-4-4"
                  ></path>
                </svg>
              </a>
              <ul className="header-menu-desktop-dropdown">
                <li className="header-menu-desktop-dropdown__item">
                  <a href="https://www.domain.com.au/" target="_self">
                    <span>Buy</span>
                  </a>
                </li>
                <li className="header-menu-desktop-dropdown__item">
                  <a target="_self">
                    <span>Rent</span>
                  </a>
                </li>
                <li className="header-menu-desktop-dropdown__item">
                  <a href="https://www.domain.com.au/new-homes" target="_self">
                    <span>New Homes</span>
                  </a>
                </li>
                <li className="header-menu-desktop-dropdown__item">
                  <a href="https://www.domain.com.au/rural" target="_self">
                    <span>Rural</span>
                  </a>
                </li>
                <li className="header-menu-desktop-dropdown__item">
                  <a href="https://www.domain.com.au/share" target="_self">
                    <span>Share</span>
                  </a>
                </li>
                <li className="header-menu-desktop-dropdown__item">
                  <a href="https://www.domain.com.au/schools" target="_self">
                    <span>Search by school</span>
                  </a>
                </li>
              </ul>
            </li>
            <li className="navigation__li" onClick={onLiClick}>
              <a className="list-item">
                Research
                <svg
                  viewBox="0 0 18 18"
                  aria-hidden="true"
                  className="domain-icon"
                >
                  <path
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    d="M13 7l-4 4-4-4"
                  ></path>
                </svg>
              </a>
              <ul className="header-menu-desktop-dropdown">
                <li className="header-menu-desktop-dropdown__item">
                  <a href="https://www.domain.com.au/" target="_self">
                    <span>Buy</span>
                  </a>
                </li>
                <li className="header-menu-desktop-dropdown__item">
                  <a target="_self">
                    <span>Rent</span>
                  </a>
                </li>
                <li className="header-menu-desktop-dropdown__item">
                  <a href="https://www.domain.com.au/new-homes" target="_self">
                    <span>New Homes</span>
                  </a>
                </li>
                <li className="header-menu-desktop-dropdown__item">
                  <a href="https://www.domain.com.au/rural" target="_self">
                    <span>Rural</span>
                  </a>
                </li>
                <li className="header-menu-desktop-dropdown__item">
                  <a href="https://www.domain.com.au/share" target="_self">
                    <span>Share</span>
                  </a>
                </li>
                <li className="header-menu-desktop-dropdown__item">
                  <a href="https://www.domain.com.au/schools" target="_self">
                    <span>Search by school</span>
                  </a>
                </li>
              </ul>
            </li>
            <li className="navigation__li">
              <a className="list-item">
                Find Agents
                <svg
                  viewBox="0 0 18 18"
                  aria-hidden="true"
                  className="domain-icon"
                >
                  <path
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    d="M13 7l-4 4-4-4"
                  ></path>
                </svg>
              </a>
            </li>
            <li className="navigation__li">
              <a className="list-item">
                Find Owners
                <svg
                  viewBox="0 0 18 18"
                  aria-hidden="true"
                  className="domain-icon"
                >
                  <path
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    d="M13 7l-4 4-4-4"
                  ></path>
                </svg>
              </a>
            </li>
            <li className="navigation__li">
              <a className="list-item">
                Home Loans
                <svg
                  viewBox="0 0 18 18"
                  aria-hidden="true"
                  className="domain-icon"
                >
                  <path
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    d="M13 7l-4 4-4-4"
                  ></path>
                </svg>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
}

export default NavBar;
